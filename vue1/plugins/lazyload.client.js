/* eslint-disable */

/**
 * Lazy Load VueJS Custom Directive
 * JF
 */

import Vue from 'vue'

Vue.directive('lazyload', {
  inserted: (el) => {
    function loadImage() {
      /**
       * Loading element
       */
      // const loadingElement = Array.from(el.children).find((el) =>
      //   el.classList.contains('loading')
      // )

      /**
       * {element} <img>
       */
      const imageElement = Array.from(el.children).find(
        (el) => el.nodeName === 'IMG'
      )

      /**
       * {element} <source>
       *
       * Get all source elements specifically:
       * 1. {type} "image/jpeg"
       * 2. {type} "image/webp"
       *
       */
      const sourceElements = Array.from(el.children).filter(
        (el) => el.nodeName === 'SOURCE'
      )

      if (imageElement) {
        imageElement.addEventListener('load', () => {
          setTimeout(() => {
            // loadingElement.classList.add('loaded')
            el.classList.add('loaded')
          }, 100)
        })
        imageElement.src = imageElement.dataset.urlSrc
        sourceElements.forEach((el) => {
          el.srcset = el.dataset.urlSrcset
        })
      }
    }

    function handleIntersect(entries, observer) {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          loadImage()
          observer.unobserve(el)
        }
      })
    }

    function createObserver() {
      const options = {
        root: null,
        threshold: '0',
      }
      const observer = new IntersectionObserver(handleIntersect, options)
      observer.observe(el)
    }

    // check if web browser supports IntersectionObserver with fallback
    if (window.IntersectionObserver) {
      createObserver()
    } else {
      loadImage()
    }
  },
})
