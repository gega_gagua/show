module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'components/**/*.js',
      'layouts/**/*.vue',
      'layouts/**/*.js',
      'pages/**/*.vue',
      'pages/**/*.js',
      'plugins/**/*.js',
      'nuxt.config.js',
      // TypeScript
      'plugins/**/*.ts',
      'nuxt.config.ts',
    ],
    options: {
      whitelist: ['html', 'body', 'nuxt-progress'],
    },
  },
  theme: {
    minWidth: {
      button: '21.6rem',
    },
    container: {
      padding: '2rem',
    },
    fontFamily: {
      tusker: 'Tusker-Grotesk-9800-Super, sans-serif',
      'tt-norms-pro-regular': 'TT-Norms-Pro-Regular, sans-serif',
      'tt-norms-pro-italic': 'TT-Norms-Pro-Italic, sans-serif',
      'tt-norms-pro-medium': 'TT-Norms-Pro-Medium, sans-serif',
      'tt-norms-pro-bold': 'TT-Norms-Pro-Bold, sans-serif',
      'tt-norms-pro-bold-italic': 'TT-Norms-Pro-Bold-Italic, sans-serif',
      'tt-norms-pro-demibold': 'TT-Norms-Pro-DemiBold, sans-serif',
      'tt-norms-pro-demibold-italic':
        'TT-Norms-Pro-DemiBold-Italic, sans-serif',
    },
    colors: {
      black: '#000000',
      white: '#FFFFFF',
      'light-cream': '#FBF9F6',
      // deep
      'deep-purple': '#391F56',
      'deep-yellow': '#7F4D10',
      'deep-green': '#113F35',
      'deep-orange': '#622C06',
      'deep-red': '#530026',
      'deep-blue': '#242756',
      // mid
      'mid-purple': '#693C90',
      'mid-yellow': '#F9B000',
      'mid-green': '#009D42',
      'mid-orange': '#F18538',
      'mid-red': '#CC2439',
      'mid-blue': '#4C76D2',
      'mid-gray': '#5A5A5A',
      // light
      'light-purple': '#DBD2EA',
      'light-yellow': '#FFE4B5',
      'light-green': '#C8E3CC',
      'light-orange': '#FDDEC4',
      'light-red': '#F9E4DF',
      'light-blue': '#D7DAEF',
      'light-gray': '#8B8B8B',
    },
    screens: {
      sm: '640px', // => @media (min-width: 640px) { ... }
      md: '768px', // => @media (min-width: 768px) { ... }
      lg: '1024px', // => @media (min-width: 1024px) { ... }
      xl: '1280px', // => @media (min-width: 1280px) { ... }
      xxl: '1440px', // => @media (min-width: 1440px) { ... }
    },
    fontSize: {
      'size-1': [
        '1.2rem',
        {
          lineHeight: '1.8rem',
          letterSpacing: '-0.005em',
        },
      ], // font size: 12px / leading: 18px / letter spacing: 5px;
      'size-2': [
        '1.4rem',
        {
          lineHeight: '2.1rem',
          letterSpacing: '-0.005em',
        },
      ], // font size: 14px / leading: 21px / letter spacing: 5px;
      'size-3': [
        '1.6rem',
        {
          lineHeight: '2.4rem',
          letterSpacing: '-0.005em',
        },
      ], // font size: 16px / leading: 24px / letter spacing: 5px;
      'size-4': [
        '2.0rem',
        {
          lineHeight: '3.0rem',
          letterSpacing: '-0.005em',
        },
      ], // font size: 20px / leading: 30px / letter spacing: 5px;
      'size-5': [
        '2.8rem',
        {
          lineHeight: '4.2rem',
          letterSpacing: '-0.015em',
        },
      ], // font size: 28px / leading: 42px / letter spacing: 15px;
      'size-6': [
        '3.6rem',
        {
          lineHeight: '5.4rem',
          letterSpacing: '-0.015em',
        },
      ], // font size: 36px / leading: 54px / letter spacing: 15px;
      'size-7': [
        '4.2rem',
        {
          lineHeight: '4.8rem',
          letterSpacing: '-0.015em',
        },
      ], // font size: 42px / leading: 48px / letter spacing: 15px;
      'size-8': [
        '6.4rem',
        {
          lineHeight: '7.2rem',
          letterSpacing: '-0.015em',
        },
      ], // font size: 64px / leading: 72px / letter spacing: 15px;
      'size-9': [
        '8.4rem',
        {
          lineHeight: '10rem',
          letterSpacing: '-0.015em',
        },
      ], // font size: 84px / leading: 100px / letter spacing: 15px;
      'size-10': [
        '10.8rem',
        {
          lineHeight: '13.5rem',
          letterSpacing: '-0.015em',
        },
      ], // font size: 108px / leading: 135px / letter spacing: 15px;
      'size-11': [
        '15.4rem',
        {
          lineHeight: '19.2rem',
          letterSpacing: '-0.015em',
        },
      ], // font size: 154px / leading: 192px / letter spacing: 15px;
    },
    extend: {
      spacing: {
        'size-1': '0.8rem',
        'size-2': '1.2rem',
        'size-3': '1.6rem',
        'size-4': '2.4rem',
        'size-5': '3.2rem',
        'size-6': '4.8rem',
        'size-7': '6.4rem',
        'size-8': '9.6rem',
        'size-9': '12.8rem',
        'size-10': '16rem',
        'size-11': '25.6rem',
        'size-12': '32.0rem',
      },
      backgroundImage: {
        'home-hero': "url('~assets/images/Home/hero-overlay.png')",
        'lead-text': "url('~assets/images/Home/leadText-background.png')",
        'story-section': "url('~assets/images/Home/story-background.jpg')",
        'newsletter-section':
          "url('~assets/images/Home/newsletter-background.png')",
        'donation-section':
          "url('~assets/images/Home/donation-background.jpg')",
      },
    },
  },
  variants: {},
  plugins: [],
}
