export const state = () => ({
  isPlaing: false,
  currentSong: {
    album: '',
    artist: '',
    cover_art_url: '',
    id: '',
    title: '',
    url: '',
  },
  playing: false,
  eventsChange: '',
  progressPercentageValue: '0%',
  playlist: {
    currentIndex: 0,
    songs: [
      {
        id: 1,
        title: 'Breathe',
        artist: 'Moonlight Motion',
        duration: '3:34',
        url: 'songsDemo/song4.wav',
        cover_art_url: 'songsDemo/song4.png',
      },
      {
        id: 2,
        title: 'Wow',
        artist: 'Post Malone',
        duration: '3:11',
        url:
          'https://res.cloudinary.com/dmf10fesn/video/upload/v1548882863/audio/Post_Malone_-_Wow._playvk.com.mp3',
        cover_art_url:
          'https://res.cloudinary.com/dmf10fesn/image/upload/v1548884701/audio/album%20arts/s-l300.jpg',
      },
      {
        id: 3,
        title: 'Hamble',
        artist: 'Kendrick Lamar',
        duration: '4:46',
        url:
          'https://res.cloudinary.com/dmf10fesn/video/upload/v1548884988/audio/Kendrick-Lamar-HUMBLE.mp3',
        cover_art_url:
          'https://res.cloudinary.com/dmf10fesn/image/upload/v1548884891/audio/album%20arts/FwqM2g6.png',
      },
      {
        id: 4,
        title: 'Dont Kill My Vibe',
        artist: 'Kendrick Lamar',
        duration: '4:46',
        url:
          'https://res.cloudinary.com/dmf10fesn/video/upload/v1548884972/audio/Kendrick-Lamar-Bitch-Dont-Kill-My-Vibe.mp3',
        cover_art_url:
          'https://res.cloudinary.com/dmf10fesn/image/upload/v1548885857/audio/album%20arts/ae8e88aa099173dbee78d904f035e459bfb69f5a.jpg',
      },
    ],
  },
  list: [],
})

export const mutations = {
  setCurrentSong(state, data) {
    state.currentSong = data
  },
  setPlaing(state, data) {
    state.playing = data
  },
  setProgressBar(state, data) {
    state.progressPercentageValue = data
  },
  setEventsChange(state, data) {
    state.eventsChange = data
  },
}
