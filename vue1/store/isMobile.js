import { screens } from '~/utils/tailwind.js'

export const state = () => ({
  isMobile: true,
})

export const mutations = {
  setIsMobile(state) {
    state.isMobile =
      window.innerWidth <
      parseInt(screens().find((breakpoint) => breakpoint.handle === 'lg').width)
  },
}
