import styled from 'styled-components'

export const LayoutComp = styled.div`
    padding: 0;
    margin: 0;

    @media screen and (max-width: 992px) {
        overflow: hidden;
    }
`