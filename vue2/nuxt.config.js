import path from 'path'
import fs from 'fs'
import { sortRoutes } from '@nuxt/utils'
import { getWorkRoutes, getThoughtsRoutes } from './utils/sitemap'

export default {
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',

  /**
   * To enable https
   * Replace with your localhost key and cert
   *
   * Use mkcert
   * https://github.com/FiloSottile/mkcert
   */
  server: {
    port: 3001,
    ...(process.env.NODE_ENV !== 'production' && {
      https: {
        key: fs.readFileSync(path.resolve(__dirname, process.env.KEY)),
        cert: fs.readFileSync(path.resolve(__dirname, process.env.CERT)),
      },
    }),
  },

  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title:
      'MAWD | Luxury Interior Designers & Architects | London | New York | Los Angeles',
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no',
      },
      {
        hid: 'description',
        name: 'description',
        content:
          'We are MAWD are a global interior design & architecture firm based in London, New York & Los Angeles, creating interiors for how we live, move, work and play today',
      },

      { hid: 'robots', name: 'robots', content: 'index' },

      {
        name: 'msapplication-TileColor',
        content: '#000000',
      },
      {
        name: 'theme-color',
        content: '#ffffff',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon-32x32.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-touch-icon.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon-16x16.png',
      },
      {
        rel: 'manifest',
        href: '/site.webmanifest',
      },
      {
        rel: 'mask-icon',
        href: '/safari-pinned-tab.svg',
        color: '#5bbad5',
      },
    ],
  },
  /*
   ** Global CSS
   */
  css: ['~/assets/css/index.css'],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: ['~/plugins/craft.js', '~/plugins/gtag'],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxt/http',
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
  ],

  robots: {
    UserAgent: '*',
    Allow: '/',
    Sitemap: 'https://mawd.co/sitemap.xml',
  },

  axios: {
    baseURL: 'https://api.mawd.co/api', // Used as fallback if no runtime config is provided
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    postcss: {
      plugins: {
        'postcss-import': {},
        'postcss-nested': {},
        'postcss-custom-properties': {},
      },
      preset: {
        autoprefixer: {},
      },
    },
    extend(config) {
      config.resolve.alias['@root'] = path.resolve(__dirname, './')
      config.resolve.alias['@pages'] = path.resolve(__dirname, 'pages')
      config.resolve.alias['@c'] = path.resolve(__dirname, 'components')
      config.resolve.alias['@assets'] = path.resolve(__dirname, 'assets')
      config.resolve.alias['@utils'] = path.resolve(__dirname, 'utils')
      config.resolve.alias['@store'] = path.resolve(__dirname, 'store')
      config.resolve.alias['@mixins'] = path.resolve(__dirname, 'mixins')
      config.resolve.alias['@constants'] = path.resolve(__dirname, 'constants')
      config.resolve.alias['@gql'] = path.resolve(__dirname, 'gql')
    },
    transpile: ['gsap'],
  },

  privateRuntimeConfig: {
    craftApiUrl: process.env.CRAFT_API_URL,
    craftAuthToken: process.env.CRAFT_AUTH_TOKEN,
  },

  publicRuntimeConfig: {
    livePreview: process.env.LIVE_PREVIEW === 'true',
    craftApiUrl:
      process.env.LIVE_PREVIEW === 'true'
        ? process.env.CRAFT_PREVIEW_API_URL
        : process.env.CRAFT_API_URL,
    craftAuthToken:
      process.env.LIVE_PREVIEW === 'true'
        ? process.env.CRAFT_PREVIEW_AUTH_TOKEN
        : process.env.CRAFT_AUTH_TOKEN,
  },
  router: {
    extendRoutes(routes, resolve) {
      routes.push(
        {
          name: 'OurWorkPageIndexDirectory',
          path: '/our-work/index/',
          component: resolve(__dirname, 'pages/our-work/directory.vue'),
        },
        {
          path: '/our-studio/founders/',
          redirect: '/our-studio/elliot-james/',
          // redirect: (to) => {
          //   const { hash, params, query } = to
          //   console.log(hash, params, query)
          // },
        }
        // {
        //   path: '/our-studio/founders/:slug?',
        //   redirect: '/our-studio/elliot-james/',
        //   // redirect: (to) => {
        //   //   const { hash, params, query } = to
        //   //   console.log(hash, params, query)
        //   // },
        // }
      )
      sortRoutes(routes)
    },
  },

  sitemap: {
    routes: async () => {
      let routes = []
      // const workCategories = [
      //   { title: 'live', id: 380 },
      //   { title: 'move', id: 381 },
      //   { title: 'work', id: 382 },
      //   { title: 'play', id: 383 },
      // ]

      const liveRoutes = await getWorkRoutes('live', 380)
      const moveRoutes = await getWorkRoutes('move', 381)
      const workRoutes = await getWorkRoutes('work', 382)
      const playRoutes = await getWorkRoutes('play', 383)
      const thoughtsRoutes = await getThoughtsRoutes()

      routes = [
        ...routes,
        ...liveRoutes,
        ...moveRoutes,
        ...workRoutes,
        ...playRoutes,
        ...thoughtsRoutes,
      ]

      return routes
    },
    hostname: 'https://mawd.co',
    gzip: true,
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date(),
    },
  },
}
