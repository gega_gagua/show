const axios = require('axios')
import gql from 'fake-tag'

const OurWork = gql`
  query OurWork($categoryId: [Int]) {
    entries(section: "ourWork", relatedTo: $categoryId) {
      ... on ourWork_ourWork_Entry {
        id
        slug
      }
    }
  }
`

const OurThoughts = gql`
  query OurThoughts {
    entries(section: "ourThoughts") {
      ... on ourThoughts_ourThoughts_Entry {
        id
        slug
      }
    }
  }
`

export const getWorkRoutes = async (title, cat) => {
  const routes = []
  const result = await axios({
    url: 'https://api.mawd.co/api',
    method: 'get',
    params: {
      query: OurWork,
      variables: { categoryId: cat },
    },
  })
  const { entries } = result.data.data
  routes.push(`/our-work/${title}`)
  entries.forEach((x) => {
    routes.push(`/our-work/${title}/${x.slug}`)
  })
  return routes
}

export const getThoughtsRoutes = async () => {
  const routes = []
  const result = await axios({
    url: 'https://api.mawd.co/api',
    method: 'get',
    params: {
      query: OurThoughts,
    },
  })
  const { entries } = result.data.data
  entries.forEach((x) => {
    routes.push(`/our-thoughts/${x.slug}`)
  })
  return routes
}
