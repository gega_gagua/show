import gsap from 'gsap'

import { CustomEase } from 'gsap/CustomEase'

gsap.registerPlugin(CustomEase)

export default (cssVar) => {
  const bezier = getComputedStyle(document.body)
    .getPropertyValue(cssVar)
    .replace(/cubic-bezier\(|\)/g, '')

  return CustomEase.create(cssVar, bezier)
}
