export default (timeout) => {
  const wait = new Promise((resolve) => setTimeout(() => resolve(), timeout))
  return wait
}
