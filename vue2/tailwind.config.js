module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  corePlugins: {
    textDecoration: false,
  },
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'components/**/*.js',
      'layouts/**/*.vue',
      'layouts/**/*.js',
      'pages/**/*.vue',
      'pages/**/*.js',
      'plugins/**/*.js',
      'nuxt.config.js',
      // TypeScript
      'plugins/**/*.ts',
      'nuxt.config.ts',
    ],
    options: {
      whitelist: ['html', 'body', 'nuxt-progress'],
    },
  },
  theme: {
    // Screens
    screens: {
      sm: '640px', // => @media (min-width: 640px) { ... }
      md: '768px', // => @media (min-width: 768px) { ... }
      lg: '1024px', // => @media (min-width: 1024px) { ... }
      xl: '1280px', // => @media (min-width: 1280px) { ... }
      xxl: '1440px', // => @media (min-width: 1440px) { ... }
    },
    fontFamily: {
      'euclid-medium': [
        'Euclid-Medium',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        'Helvetica',
        'Arial',
        'sans-serif',
      ],
      'euclid-regular': [
        'Euclid-Regular',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        'Helvetica',
        'Arial',
        'sans-serif',
      ],
      'euclid-semibold': [
        'Euclid-Semibold',
        'system-ui',
        '-apple-system',
        'BlinkMacSystemFont',
        'Helvetica',
        'Arial',
        'sans-serif',
      ],
      'lyon-regular': [
        'Lyon-Regular',
        'Georgia',
        'Cambria',
        '"Times New Roman"',
        'Times',
        'serif',
      ],
      'lyon-semibold': [
        'Lyon-Semibold',
        'Georgia',
        'Cambria',
        '"Times New Roman"',
        'Times',
        'serif',
      ],
    },
    gap: {
      xxs: '0.594rem', // 9.50px
      xs: '1.188rem', // 19px
      s: '2.375rem', // 38px
      m: '3.563rem', // 57px
      l: '4.75rem', // 76px
      xl: '7.125rem', // 114px
      xxl: '9.5rem', // 152px
      xxxl: '14.25rem', // 228px
      'gutter-mob': '0.9375rem', // 15px
      'gutter-desk': '1.813rem', // 29px
      0: '0',
    },
    fontSize: {
      // final type sizes supplied on 07 Oct 2020
      // {{ font size / leading }}
      'mob-size-1': [
        '0.875rem',
        {
          lineHeight: '1.25rem',
          letterSpacing: '-0.01em',
        },
      ], // 14px / 20px
      'mob-size-2': [
        '3.375rem',
        {
          lineHeight: '3.563rem',
          letterSpacing: '-0.03em',
        },
      ], // 54px / 57px

      'size-1': [
        '0.75rem',
        {
          lineHeight: '1.125rem',
          letterSpacing: '-0.01em',
        },
      ], // 12px / 18px
      'size-2': [
        '1.125rem',
        {
          lineHeight: '1.5rem',
          letterSpacing: '-0.01em',
        },
      ], // 18px / 24px
      'size-3': [
        '1.313rem',
        {
          lineHeight: '1.875rem',
          letterSpacing: '-0.01em',
        },
      ], // 21px / 30px
      'size-4': [
        '1.5rem',
        {
          lineHeight: '1.875rem',
          letterSpacing: '-0.02em',
        },
      ], // 24px / 30px
      'size-5': [
        '2.25rem',
        {
          lineHeight: '2.625rem',
          letterSpacing: '-0.02em',
        },
      ], // 36px / 42px
      'size-6': [
        '3rem',
        {
          lineHeight: '3.375rem',
          letterSpacing: '-0.03em',
        },
      ], // 48px / 54px
      'size-7': [
        '6rem',
        {
          lineHeight: '6.75rem',
          letterSpacing: '-0.03em',
        },
      ], // 96px / 108px
      'size-8': [
        '7.5rem',
        {
          lineHeight: '7.875rem',
          letterSpacing: '-0.03em',
        },
      ], // 120px / 126px
      'size-9': [
        '9.75rem',
        {
          lineHeight: '10.125rem',
          letterSpacing: '-0.03em',
        },
      ], // 156px / 162px
      'size-10': [
        '12.75rem',
        {
          lineHeight: '13.125rem',
          letterSpacing: '-0.03em',
        },
      ], // 204px / 210px

      // Our Thoughts
      'thoughts-1': [
        '1rem',
        {
          lineHeight: '1.313rem',
          letterSpacing: '-0.01em',
        },
      ], // 16px / 21px
      'thoughts-2': [
        '1.313rem',
        {
          lineHeight: '1.875rem',
          letterSpacing: '-0.01em',
        },
      ], // 21px / 30px
      'thoughts-3': [
        '1.5rem',
        {
          lineHeight: '2.063rem',
          letterSpacing: '-0.02em',
        },
      ], // 24px / 33px
      'thoughts-4': [
        '1.875rem',
        {
          lineHeight: '2.438rem',
          letterSpacing: '-0.02em',
        },
      ], // 30px / 39px
      'thoughts-5': [
        '2.625rem',
        {
          lineHeight: '3rem',
          letterSpacing: '-0.02em',
        },
      ], // 42px / 48px
      'thoughts-6': [
        '6rem',
        {
          lineHeight: '6.75rem',
          letterSpacing: '-0.03em',
        },
      ], // 96px / 108px
    },
    letterSpacing: {
      s: '-0.01em', // type sizes below and including 2.25rem //
      m: '-0.02em', // type sizes above 2.25rem // -3.2px
      l: '-0.03em', // type sizes above 7.5rem
    },
    colors: {
      white: '#FFFFFF',
      'dark-grey': '#707070',
      black: '#000000',
      'light-grey': '#CCCCCC',
      grey: '#575757',
      red: 'red',
      yellow: 'yellow',
      putty: '#f7f7f7',
    },
    opacity: {
      0: '0',
      25: '.25',
      75: '.75',
      10: '.1',
      20: '.2',
      30: '.3',
      40: '.4',
      50: '.5',
      60: '.6',
      70: '.7',
      80: '.8',
      90: '.9',
      100: '1',
    },
    extend: {
      zIndex: {
        60: '60',
      },
      lineHeight: {
        'year-digits': '9.4rem',
      },
      inset: {
        xxxs: '0.297rem', // 4.75px
        xxs: '0.594rem', // 9.50px
        xs: '1.188rem', // 19px
        s: '2.375rem', // 38px
        m: '3.563rem', // 57px
        l: '4.75rem', // 76px
        xl: '7.125rem', // 114px
        xxl: '9.5rem', // 152px
        xxxl: '14.25rem', // 228px
      },
      minHeight: {
        paragraph: '15rem',
      },
      spacing: {
        xxxs: '0.297rem', // 4.75px
        xxs: '0.594rem', // 9.50px
        xs: '1.188rem', // 19px
        s: '2.375rem', // 38px
        m: '3.563rem', // 57px
        l: '4.75rem', // 76px
        xl: '7.125rem', // 114px
        xxl: '9.5rem', // 152px
        xxxl: '14.25rem', // 228px
        'gutter-mob': '0.9375rem', // 15px
        'gutter-desk': '1.813rem', // 29px
        'gutter-mob-1/2': '0.46875rem',
        'gutter-desk-1/2': '0.9065rem',
        // auto: 'auto', temporary disabled this as nuxt-build was not working
        0: '0',
        'view-all': '2.625rem', // 42px
      },
      width: {
        'screen-1/2': '50vw',
        'screen-3/5': '60vw',
      },
      gridTemplateColumns: {
        // 18 column grid
        13: 'repeat(13, minmax(0, 1fr))',
        14: 'repeat(14, minmax(0, 1fr))',
        15: 'repeat(15, minmax(0, 1fr))',
        16: 'repeat(16, minmax(0, 1fr))',
        17: 'repeat(17, minmax(0, 1fr))',
        18: 'repeat(18, minmax(0, 1fr))',
      },
      gridColumn: {
        'span-14': 'span 14 / span 14',
        'span-15': 'span 15 / span 15',
        'span-16': 'span 16 / span 16',
        'span-17': 'span 17 / span 17',
        'span-18': 'span 18 / span 18',
      },
      gridColumnStart: {
        14: '14',
        15: '15',
        16: '16',
        17: '17',
        18: '18',
      },
      gridColumnEnd: {
        14: '14',
        15: '15',
        16: '16',
        17: '17',
        18: '18',
      },
      // rows
      gridTemplateRows: {
        8: 'repeat(8, minmax(0, 1fr))',
        9: 'repeat(9, minmax(0, 1fr))',
        10: 'repeat(10, minmax(0, 1fr))',
        11: 'repeat(11, minmax(0, 1fr))',
        12: 'repeat(12, minmax(0, 1fr))',
        13: 'repeat(13, minmax(0, 1fr))',
        14: 'repeat(14, minmax(0, 1fr))',
        15: 'repeat(15, minmax(0, 1fr))',
        16: 'repeat(16, minmax(0, 1fr))',
        17: 'repeat(17, minmax(0, 1fr))',
        18: 'repeat(18, minmax(0, 1fr))',
      },
      gridRow: {
        'span-8': 'span 8 / span 8',
        'span-9': 'span 9 / span 9',
        'span-10': 'span 10 / span 10',
        'span-11': 'span 11 / span 11',
        'span-12': 'span 12 / span 12',
        'span-13': 'span 13 / span 13',
        'span-14': 'span 14 / span 14',
        'span-15': 'span 15 / span 15',
        'span-16': 'span 16 / span 16',
        'span-17': 'span 17 / span 17',
        'span-18': 'span 18 / span 18',
      },
      gridRowStart: {
        8: '8',
        9: '9',
        10: '10',
        11: '11',
        12: '12',
        13: '13',
        14: '14',
        15: '15',
        16: '16',
        17: '17',
        18: '18',
      },
      gridRowEnd: {
        8: '8',
        9: '9',
        10: '10',
        11: '11',
        12: '12',
        13: '13',
        14: '14',
        15: '15',
        16: '16',
        17: '17',
        18: '18',
      },
    },
  },
  variants: {},
  plugins: [],
}
