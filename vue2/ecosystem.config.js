module.exports = {
  apps: [
    {
      name: 'MAWD',
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start',
      instances: 'max',
      exec_mode: 'cluster',
      max_memory_restart: '512M',
      exp_backoff_restart_delay: 100,
      watch: false,
      env: {
        PORT: 3001,
        NODE_ENV: 'production',
      },
    },
  ],
}
