/* eslint-disable graphql/template-strings */
import gql from 'fake-tag'
import { media, base, seo } from '@gql/fragments'

// FRAGMENTS
export const ourThoughtsCategory = gql`
  fragment ourThoughtsCategory on ourThoughts_Category {
    id
    title
  }
`

export const ourThoughtsContent = gql`
  fragment ourThoughtsContent on ourThoughts_ourThoughts_Entry {
    ourThoughtsContent @include(if: $content) {
      __typename: __typename
      ... on ourThoughtsContent_text_BlockType {
        text
        subHeading
      }
      ... on ourThoughtsContent_doublePortraitImage_BlockType {
        caption0
        caption1
        image0 {
          ...media
        }
        image1 {
          ...media
        }
      }
      ... on ourThoughtsContent_portraitImage_BlockType {
        image {
          ...media
        }
        caption
      }
      ... on ourThoughtsContent_landscapeImage_BlockType {
        image {
          ...media
        }
        caption
      }
      ... on ourThoughtsContent_quote_BlockType {
        text
      }
      ... on ourThoughtsContent_video_BlockType {
        videoUrl
      }
    }
    ourThoughtsSimilarArticles @include(if: $content) {
      ...ourThoughtsBase
    }
  }
`

export const ourThoughtsEntry = gql`
  fragment ourThoughtsEntry on ourThoughts_ourThoughts_Entry {
    ...ourThoughtsBase
    ...ourThoughtsContent
  }
`

export const ourThoughtsBase = gql`
  fragment ourThoughtsBase on ourThoughts_ourThoughts_Entry {
    ...base
    date
    tags: ourThoughtsTags {
      ...ourThoughtsCategory
    }
    media {
      ...media
    }
    ourThoughtsAuthor
    text
  }
`

export const ourThoughtsBootstrap = gql`
  fragment ourThoughtsBootstrap on Query {
    bootstrap: entries(section: "ourThoughts", orderBy: "date") {
      id
    }
  }
`

export const ourThoughtsLatest = gql`
  fragment ourThoughtsLatest on Query {
    latest: entries(
      limit: $limit
      section: "ourThoughts"
      slug: $not
      orderBy: "date"
    ) {
      ... on ourThoughts_ourThoughts_Entry {
        ...ourThoughtsEntry
      }
    }
  }
`

// QUERIES
export const OurThoughtsIndexQuery = gql`
  query OurThoughtsIndexQuery($content: Boolean = false) {
    entries(section: "ourThoughts", orderBy: "date") {
      ...ourThoughtsEntry
    }

    categories {
      ...ourThoughtsCategory
    }

    ...ourThoughtsBootstrap

    page: entry(section: "ourThoughtsPage") {
      ... on ourThoughtsPage_ourThoughtsPage_Entry {
        id
        seomatic(asArray: true) {
          ...seo
        }
      }
    }
  }

  ${base}
  ${seo}
  ${media}
  ${ourThoughtsBase}
  ${ourThoughtsCategory}
  ${ourThoughtsContent}
  ${ourThoughtsEntry}
  ${ourThoughtsBootstrap}
`

// If craft implements the @exclude directive use that on id to derive the not
// If craft correctly implements it's type for slug to not be an array, use that
export const OurThoughtsEntryQuery = gql`
  query OurThoughtsEntry($slug: [String], $content: Boolean = true) {
    entry(slug: $slug) {
      ...ourThoughtsEntry

      seomatic(asArray: true) {
        ...seo
      }
    }

    ...ourThoughtsBootstrap
  }

  ${base}
  ${seo}
  ${media}
  ${ourThoughtsBase}
  ${ourThoughtsCategory}
  ${ourThoughtsContent}
  ${ourThoughtsEntry}
  ${ourThoughtsBootstrap}
`

export const OurThoughtsLatestQuery = gql`
  query OurThoughtsLatestQuery(
    $limit: Int
    $not: [String] = []
    $content: Boolean = false
  ) {
    ...ourThoughtsLatest
  }

  ${base}
  ${media}
  ${ourThoughtsBase}
  ${ourThoughtsEntry}
  ${ourThoughtsContent}
  ${ourThoughtsCategory}
  ${ourThoughtsLatest}
`
