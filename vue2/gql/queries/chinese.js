/* eslint-disable graphql/template-strings */
import gql from 'fake-tag'
import { media } from '@gql/fragments'
import { ourWorkCategoryImage } from './ourWork'
export const ChineseHomePageQuery = gql`
  query ChineseHomePageQuery {
    entry(site: "mawdChinese", section: "homepageChinese") {
      ... on homepageChinese_homepageChinese_Entry {
        id
        title
        about {
          title: body
        }
        excdEntries {
          id
          slug
          title
        }
        text
        body
        media {
          ...media
        }
        multiText
        ourValues
      }
    }
    ourWorkSelected: categories(group: "ourWork", site: "mawdChinese") {
      id
      slug
      title
      ... on ourWork_Category {
        featuredProject {
          title
          uri
          ... on ourWork_ourWork_Entry {
            location {
              title
            }
          }
          media {
            ...ourWorkCategoryImage
          }
        }
      }
    }
  }
  ${media}
  ${ourWorkCategoryImage}
`

export const EXCDPageEntries = gql`
  query EXCDPageEntries {
    entries(site: "mawdChinese", section: "experienceCentricDesign") {
      ... on experienceCentricDesign_experienceCentricDesign_Entry {
        id
        title
        multiText
        image {
          ...media
        }
      }
    }
  }
  ${media}
`

export const EXCDPageEntry = gql`
  query EXCDPageEntry($slug: [String]) {
    entry(
      site: "mawdChinese"
      section: "experienceCentricDesign"
      slug: $slug
    ) {
      ... on experienceCentricDesign_experienceCentricDesign_Entry {
        id
        title
        multiText
        media {
          ...media
        }
      }
    }
  }
  ${media}
`

export const ClientsEntries = gql`
  query ClientsEntries {
    entries(site: "mawdChinese", section: "clientsChinese") {
      ... on clientsChinese_clientsChinese_Entry {
        title
        media {
          id
          title
          ... on mawdS3Media_Asset {
            image: imageCover {
              src
              srcset
              srcWebp
              srcsetWebp
              placeholderImage
              placeholderBox
            }
          }
        }
      }
    }
  }
`

export const ClientsTestimonials = gql`
  query ClientsTestimonials {
    entries(site: "mawdChinese", section: "testimonials") {
      ... on testimonials_testimonials_Entry {
        id
        title
        multiText
        text
        media {
          ... on mawdS3Media_Asset {
            image: imageCover {
              src
              srcset
              srcWebp
              srcsetWebp
              placeholderImage
              placeholderBox
            }
          }
        }
      }
    }
  }
`
