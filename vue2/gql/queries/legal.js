import gql from 'fake-tag'
import { base } from '@gql/fragments'

export const LegalQuery = gql`
  query LegalQuery {
    entry(section: "legal", limit: 1) {
      ...base
      ... on legal_legal_Entry {
        body
      }
    }
  }
  ${base}
`
