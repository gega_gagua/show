import gql from 'fake-tag'
import { ourThoughtsCategory } from './ourThoughts'

export const BurgerMenuQuery = gql`
  query BurgerMenuQuery {
    categories {
      ...ourThoughtsCategory
    }
    pressMedia: entries(section: "pressAndMedia", orderBy: "date DESC") {
      ... on pressAndMedia_pressAndMedia_Entry {
        year: date
      }
    }
  }

  ${ourThoughtsCategory}
`
