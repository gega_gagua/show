import gql from 'fake-tag'
import { media, media3x2, media4x5, base, seo } from '@gql/fragments'
import {
  blockStyleA,
  blockStyleB,
  blockStyleC,
  blockStyleD,
  soloSplitBlock,
  soloFullBlock,
} from './ourWork'

export const OurStudioValuesQuery = gql`
  query OurStudioValuesQuery {
    entry(section: "ourStudioValues") {
      ...base
      ... on ourStudioValues_ourStudioValues_Entry {
        multiText
        ourStudioValues {
          ... on ourStudioValues_ourValue_BlockType {
            id
            text
            heading
          }
        }
      }
      seomatic(asArray: true) {
        ...seo
      }
    }
  }
  ${seo}
  ${base}
`

export const OurStudioPeopleQuery = gql`
  query OurStudioPeopleQuery {
    entry(section: "ourStudioPeople") {
      ...base
      ... on ourStudioPeople_ourStudioPeople_Entry {
        multiText
        ourStudioPeople {
          ... on ourStudioPeople_person_BlockType {
            personName
            bio
            additionalInfo
            image {
              ...media
            }
          }
        }
      }
      seomatic(asArray: true) {
        ...seo
      }
    }
  }

  ${seo}
  ${base}
  ${media}
`

export const OurStudioCareersQuery = gql`
  query OurStudioCareersQuery {
    entry(section: "ourStudioCareers") {
      ...base
      ... on ourStudioCareers_ourStudioCareers_Entry {
        multiText
        ourStudioCareers {
          ... on ourStudioCareers_jobListing_BlockType {
            id
            salary
            position
            dateAdded
            applicationsClose
            contract
            locaton
            description
            howToApply
          }
        }
      }
      seomatic(asArray: true) {
        ...seo
      }
    }
  }

  ${seo}
  ${base}
`

export const OurStudioClientsQuery = gql`
  query OurStudioClientsQuery {
    entry(section: "ourStudioClients") {
      ...base
      ... on ourStudioClients_ourStudioClients_Entry {
        ourStudioClientQuotes {
          ... on ourStudioClientQuotes_quote_BlockType {
            company
            personName
            quote
          }
        }
      }
      seomatic(asArray: true) {
        ...seo
      }
    }
  }

  ${seo}
  ${base}
`

export const OurStudioElliotJamesQuery = gql`
  query OurStudioElliotJamesQuery {
    entry(section: "ourStudioElliotJames") {
      ...base
      ... on ourStudioElliotJames_ourStudioElliotJames_Entry {
        multiText
        elliotJames {
          ... on elliotJames_person_BlockType {
            bio
            expandText
            image {
              ...media
            }
            personName
          }
        }
      }
      seomatic(asArray: true) {
        ...seo
      }
    }
  }

  ${seo}
  ${base}
  ${media}
`

// Separate Elliot March and James White Bio pages
// {url} /elliot-james/elliot-march
// {url} /elliot-james/james-white
// {single section}
export const OurStudioElliotMarchQuery = gql`
  query OurStudioElliotMarchQuery {
    entry(section: "elliotMarch") {
      ...base
      ... on elliotMarch_elliotMarch_Entry {
        contentStyles {
          ...blockStyleA
          ...blockStyleB
          ...blockStyleC
          ...blockStyleD
          ...soloSplitBlock
          ...soloFullBlock
        }
      }
    }
  }
  ${base}
  ${media4x5}
  ${media3x2}
  ${blockStyleA}
  ${blockStyleB}
  ${blockStyleC}
  ${blockStyleD}
  ${soloSplitBlock}
  ${soloFullBlock}
`

export const OurStudioJamesWhiteQuery = gql`
  query OurStudioJamesWhiteQuery {
    entry(section: "jamesWhite") {
      ...base
      ... on jamesWhite_jamesWhite_Entry {
        contentStyles {
          ...blockStyleA
          ...blockStyleB
          ...blockStyleC
          ...blockStyleD
          ...soloSplitBlock
          ...soloFullBlock
        }
      }
    }
  }
  ${base}
  ${media4x5}
  ${media3x2}
  ${blockStyleA}
  ${blockStyleB}
  ${blockStyleC}
  ${blockStyleD}
  ${soloSplitBlock}
  ${soloFullBlock}
`
