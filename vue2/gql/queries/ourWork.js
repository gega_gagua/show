/* eslint-disable graphql/template-strings */
import gql from 'fake-tag'
import { media, media4x5, media3x2, base, seo } from '@gql/fragments'

// FRAGMENTS

export const ourWorkCategoryImage = gql`
  fragment ourWorkCategoryImage on mawdS3Media_Asset {
    title
    imageOptimise {
      src
      srcset
      srcWebp
      srcsetWebp
      placeholderImage
      placeholderBox
    }
  }
`

export const ourWorkCategory = gql`
  fragment ourWorkCategory on ourWork_Category {
    id
    slug
    title
    featuredProject {
      title
      uri
      ... on ourWork_ourWork_Entry {
        location {
          title
        }
      }
      media {
        ...ourWorkCategoryImage
      }
    }
  }
`

export const blockStyleA = gql`
  fragment blockStyleA on contentStyles_styleA_BlockType {
    typeHandle
    heading
    text
    imageLeft {
      ...media4x5
    }
    imageLeftCaption
    imageRight {
      ...media4x5
    }
    imageRightCaption
    quote {
      quote: col1
      cite: col2
      externalLink: col3
    }
  }
`

export const blockStyleB = gql`
  fragment blockStyleB on contentStyles_styleB_BlockType {
    typeHandle
    image {
      ...media3x2
    }
    caption
  }
`

export const blockStyleC = gql`
  fragment blockStyleC on contentStyles_styleC_BlockType {
    typeHandle
    headingTop
    textTop
    imageLeft {
      ...media3x2
    }
    imageLeftCaption
    imageRight {
      ...media4x5
    }
    imageRightCaption
    headingBottom
    textBottom
  }
`

export const blockStyleD = gql`
  fragment blockStyleD on contentStyles_styleD_BlockType {
    typeHandle
    heading
    text
    imageLeft {
      ...media3x2
    }
    imageLeftCaption
    imageRight {
      ...media4x5
    }
    imageRightCaption
  }
`

export const soloSplitBlock = gql`
  fragment soloSplitBlock on contentStyles_styleSoloSplit_BlockType {
    typeHandle
    text
    imageRight {
      ...media4x5
    }
    quote {
      quote: col1
      cite: col2
      externalLink: col3
    }
  }
`

export const soloFullBlock = gql`
  fragment soloFullBlock on contentStyles_styleSoloFull_BlockType {
    typeHandle
    text
    image {
      ...media3x2
    }
    quote {
      quote: col1
      cite: col2
      externalLink: col3
    }
  }
`

export const styleVideo = gql`
  fragment styleVideo on contentStyles_styleVideo_BlockType {
    typeHandle
    video: videoUrls {
      width: col1
      videoUrl: col2
    }
  }
`

export const projectDetails = gql`
  fragment projectDetails on ourWork_ourWork_Entry {
    location {
      title
    }
    media {
      ...media
    }
    category: ourWorkCategory {
      slug
    }
  }
`

export const ourWorkContent = gql`
  fragment ourWorkContent on ourWork_ourWork_Entry {
    __typename: __typename
    location {
      title
    }
    meta {
      title: col1
      label: col2
    }
    contentStyles {
      ...blockStyleA
      ...blockStyleB
      ...blockStyleC
      ...blockStyleD
      ...soloSplitBlock
      ...soloFullBlock
      ...styleVideo
    }
    relatedPressAndMedia {
      title
      ... on pressAndMedia_pressAndMedia_Entry {
        externalLink
        pressAndMediaOutlet
      }
    }
    additionalInformation {
      title: col1
      label: col2
      link: col3
    }
    similarProjects {
      title
      slug
      ...projectDetails
    }
  }
`

// QUERIES
export const OurWorkIndexQuery = gql`
  query OurWorkIndexQuery($content: Boolean = false) {
    entries(section: "ourWork", orderBy: "date") {
      ...base
      ...ourWorkContent
    }

    categories {
      ...ourWorkCategory
    }
  }
  ${media}
  ${base}
  ${ourWorkContent}
  ${ourWorkCategory}
`

// QUERIES
export const OurWorkCategoriesQuery = gql`
  query OurWorkCategoriesQuery {
    categories(group: "ourWork") {
      ...ourWorkCategory
    }
    page: entry(section: "projectsPage") {
      ... on projectsPage_projectsPage_Entry {
        id
        seomatic(asArray: true) {
          ...seo
        }
      }
    }
  }

  ${seo}
  ${ourWorkCategory}
  ${ourWorkCategoryImage}
`

export const OurWorkEntries = gql`
  query OurWorkEntries($categoryId: [Int], $site: [String] = "default") {
    entries(section: "ourWork", relatedTo: $categoryId, site: $site) {
      title
      slug
      ... on ourWork_ourWork_Entry {
        categories: ourWorkCategory {
          id
          slug
          title
        }
        location {
          title
        }
        media {
          ... on mawdS3Media_Asset {
            title
            image3x2: imageRatio3By2 {
              src
              srcset
              srcWebp
              srcsetWebp
              placeholderImage
              placeholderBox
            }
          }
        }
      }
    }
    categories(group: "ourWork", site: $site) {
      id
      slug
      title
      ... on ourWork_Category {
        paragraph: multiText
      }
      seomatic(asArray: true) {
        ...seo
      }
    }
    seomatic(asArray: true) {
      ...seo
    }
  }

  ${seo}
`

export const OurWorkCategoryQuery = gql`
  query OurWorkCategoryQuery($slug: [String], $site: [String] = "default") {
    category(group: "ourWork", slug: $slug, site: $site) {
      id
      slug
      title
      seomatic(asArray: true) {
        ...seo
      }
    }
  }
  ${seo}
`

// If craft implements the @exclude directive use that on id to derive the not
// If craft correctly implements it's type for slug to not be an array, use that
export const OurWorkEntryQuery = gql`
  query OurWorkEntry($slug: [String], $site: [String] = "default") {
    entry(slug: $slug, site: $site) {
      ...base
      ...ourWorkContent
      seomatic(asArray: true) {
        ...seo
      }
    }
  }
  ${ourWorkContent}
  ${media4x5}
  ${media3x2}
  ${media}
  ${base}
  ${seo}
  ${blockStyleA}
  ${blockStyleB}
  ${blockStyleC}
  ${blockStyleD}
  ${soloSplitBlock}
  ${soloFullBlock}
  ${styleVideo}
  ${projectDetails}
`
