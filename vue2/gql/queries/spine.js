/* eslint-disable graphql/template-strings */
import gql from 'fake-tag'
import { base, media, seo } from '@gql/fragments'
import {
  ourThoughtsBase,
  ourThoughtsEntry,
  ourThoughtsContent,
  ourThoughtsCategory,
  ourThoughtsBootstrap,
  ourThoughtsLatest,
} from './ourThoughts'
import { ourWorkCategoryImage } from './ourWork'

export const SpineQuery = gql`
  query SpineOurThoughtsQuery(
    $content: Boolean = false
    $limit: Int
    $not: [String] = []
  ) {
    homepage: entry(section: "homepage") {
      ... on homepage_homepage_Entry {
        ourThoughts {
          ...ourThoughtsEntry
        }
        videoMobile: homepageVideoMobile {
          ... on homepageVideoMobile_videoSize_BlockType {
            id
            videoUrl
            width
          }
        }
        videoDesktop: homepageVideoDesktop {
          ... on homepageVideoDesktop_videoSize_BlockType {
            id
            videoUrl
            width
          }
        }
        videoOurProcessMobile: homepageOurProcessVideoMobile {
          ... on homepageOurProcessVideoMobile_videoSize_BlockType {
            id
            videoUrl
            width
          }
        }
        videoOurProcessDesktop: homepageOurProcessVideoDesktop {
          ... on homepageOurProcessVideoDesktop_videoSize_BlockType {
            id
            videoUrl
            width
          }
        }
      }
    }

    ...ourThoughtsLatest
    ...ourThoughtsBootstrap

    overview: entry(section: "overview") {
      ... on overview_overview_Entry {
        id
        multiText
      }
    }

    seomatic(uri: "/", asArray: true) {
      ...seo
    }

    ourWorkSelected: categories(group: "ourWork") {
      id
      slug
      title
      ... on ourWork_Category {
        featuredProject {
          title
          uri
          ... on ourWork_ourWork_Entry {
            location {
              title
            }
          }
          media {
            ...ourWorkCategoryImage
          }
        }
      }
    }
  }

  ${base}
  ${media}
  ${seo}
  ${ourThoughtsBase}
  ${ourThoughtsEntry}
  ${ourThoughtsContent}
  ${ourThoughtsCategory}
  ${ourThoughtsBootstrap}
  ${ourThoughtsLatest}
  ${ourWorkCategoryImage}
`
