import gql from 'fake-tag'

export const media = gql`
  fragment media on mawdS3Media_Asset {
    id
    title
    image: imageCover {
      src
      srcset
      srcWebp
      srcsetWebp
      placeholderImage
      placeholderBox
    }
    image3x2: imageRatio3By2 {
      src
      srcset
      srcWebp
      srcsetWebp
      placeholderImage
      placeholderBox
    }
    image4x5: imageRatio4By5 {
      src
      srcset
      srcWebp
      srcsetWebp
      placeholderImage
      placeholderBox
    }
  }
`

export const media3x2 = gql`
  fragment media3x2 on mawdS3Media_Asset {
    id
    title
    imageRatio3By2 {
      src
      srcset
      srcWebp
      srcsetWebp
      placeholderImage
      placeholderBox
    }
  }
`

export const media4x5 = gql`
  fragment media4x5 on mawdS3Media_Asset {
    id
    title
    imageRatio4By5 {
      src
      srcset
      srcWebp
      srcsetWebp
      placeholderImage
      placeholderBox
    }
  }
`

export const base = gql`
  fragment base on EntryInterface {
    id
    slug
    title
  }
`

export const seo = gql`
  fragment seo on SeomaticType {
    metaTitleContainer
    metaTagContainer
    metaLinkContainer
    metaJsonLdContainer
  }
`
