export default function (to, from, savedPosition) {
  return new Promise((resolve, reject) => {
    if (to.hash) {
      resolve({
        selector: to.hash,
      })
    }
    setTimeout(() => {
      resolve({ x: 0, y: 0 })
    }, 1000)
  })
}
