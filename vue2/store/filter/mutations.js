export const MUTATIONS = {
  ADD_CATEGORY: 'ADD_CATEGORY',
  REMOVE_CATEGORY: 'REMOVE_CATEGORY',
  CLEAR_CATEGORY: 'CLEAR_CATEGORY',
  ADD_SUB_CATEGORY: 'ADD_SUB_CATEGORY',
  REMOVE_SUB_CATEGORY: 'REMOVE_SUB_CATEGORY',
  CLEAR_SUB_CATEGORY: 'CLEAR_SUB_CATEGORY',
  SET_ACTIVE_RANGE_INDEX: 'SET_ACTIVE_RANGE_INDEX',
  UNSET_ACTIVE_RANGE_INDEX: 'UNSET_ACTIVE_RANGE_INDEX',
  CLEAR_FILTERS: 'CLEAR_FILTERS',
}

export default {
  /**
   * Add category to tags
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.ADD_CATEGORY](state, payload) {
    state.categories.length = 0
    state.categories.push(payload)
  },

  /**
   * Remove category to tags
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.REMOVE_CATEGORY](state, payload) {
    const index = state.categories.indexOf(payload)
    state.categories.splice(index, 1)
  },

  /**
   * Remove category to tags
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.CLEAR_CATEGORY](state) {
    state.categories = []
  },

  /**
   * Add category to tags
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.ADD_SUB_CATEGORY](state, payload) {
    state.subCategories.length = 0
    state.subCategories.push(payload)
  },

  /**
   * Remove category to tags
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.REMOVE_SUB_CATEGORY](state, payload) {
    const index = state.categories.indexOf(payload)
    state.subCategories.splice(index, 1)
  },

  /**
   * Remove category to tags
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.CLEAR_SUB_CATEGORY](state) {
    state.subCategories = []
  },

  /**
   * Set Active Range Index
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.SET_ACTIVE_RANGE_INDEX](state, payload) {
    // console.log(payload, state)
    state.activeRangeIndex = payload
  },

  /**
   * Set Active Range Index
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.UNSET_ACTIVE_RANGE_INDEX](state) {
    state.activeRangeIndex = null
  },
}
