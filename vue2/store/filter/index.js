import { MUTATIONS } from './mutations'
require('polyfill-object.fromentries')

// Because Nuxt does not respect the namespaced property when keys are broken up
// into separate files this is a hacky workaround for now
// https://github.com/nuxt/nuxt.js/issues/8130

const indexToNamespaced = (indexs) =>
  Object.fromEntries(
    Object.entries(indexs).map(([action, index]) => [
      action,
      `${namespace}/${index}`,
    ])
  )

const namespace = 'filter'

const NAMESPACED_MUTATIONS = indexToNamespaced(MUTATIONS)

export { NAMESPACED_MUTATIONS as MUTATIONS }
