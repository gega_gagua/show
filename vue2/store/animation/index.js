let requestId = null

const GETTERS = {
  SUBSCRIPTIONS: 'SUBSCRIPTIONS',
}

const MUTATIONS = {
  SUBSCRIBE: 'SUBSCRIBE',
  UNSUBSCRIBE: 'UNSUBSCRIBE',
}

export const ACTIONS = {
  SUBSCRIBE: 'SUBSCRIBE',
  UNSUBSCRIBE: 'UNSUBSCRIBE',
}

/**
 * The magic loop
 */
function animationFrame() {
  for (let i = 0, l = this().length; i < l; i += 1) {
    this()[i]()
  }

  requestId = window.requestAnimationFrame(animationFrame.bind(this))
}

export default {
  namespaced: false,

  state: () => ({
    uuid: 0,
    animationSubscriptions: new Map(),
    animationSubscriptionsChangeTracker: 1,
  }),

  getters: {
    [GETTERS.SUBSCRIPTIONS](state) {
      // uuid makes clockSubscriptions reactive
      if (state.animationSubscriptionsChangeTracker) {
        return Array.from(state.animationSubscriptions.values())
      }

      return []
    },
  },

  mutations: {
    [MUTATIONS.SUBSCRIBE](state, fn) {
      state.animationSubscriptions.set((state.uuid += 1), fn)
      state.animationSubscriptionsChangeTracker += 1
    },

    [MUTATIONS.UNSUBSCRIBE](state, uuid) {
      state.animationSubscriptions.delete(uuid)
      state.animationSubscriptionsChangeTracker += 1
    },
  },

  actions: {
    [ACTIONS.SUBSCRIBE]: ({ commit, getters, state }, fn) => {
      if (process.server) return

      commit(MUTATIONS.SUBSCRIBE, fn)
      const { uuid } = state

      // Start the clock if this is the first thing added to the stack
      if (getters[GETTERS.SUBSCRIPTIONS].length === 1) {
        animationFrame.call(() => getters[GETTERS.SUBSCRIPTIONS])
      }

      return uuid
    },

    [ACTIONS.UNSUBSCRIBE]({ commit, getters }, uuid) {
      if (process.server) return

      commit(MUTATIONS.UNSUBSCRIBE, uuid)

      // Stop the clock if the stack is empty
      if (getters[GETTERS.SUBSCRIPTIONS].length === 0 && process.client) {
        window.cancelAnimationFrame(requestId)
        requestId = null
      }
    },
  },
}
