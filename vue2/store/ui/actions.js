import { MUTATIONS } from './mutations'

export const ACTIONS = {
  SET_SCREEN_HANDLERS: 'SET_SCREEN_HANDLERS',
}

export default {
  /**
   * Convenience action to set all screen handlers at once
   */
  [ACTIONS.SET_SCREEN_HANDLERS]({ commit }) {
    commit(MUTATIONS.SET_IS_MOBILE)
    commit(MUTATIONS.SET_DEVICE_WIDTH)
    commit(MUTATIONS.SET_DEVICE_HEIGHT)
    commit(MUTATIONS.SET_DPR)
  },
}
