export default () => ({
  deviceWidth: 750,
  deviceHeight: 600,
  dpr: 2,
  isMobile: true,
  menuOpen: false,
  cursorSignpost: null,
  cursorInteracting: false,
  cursorSignpostColor: null,
  breadcrumb: null,
  menuBgColor: null,
  wheelEnabled: false,
  touchEnabled: false,
  scrollData: null,
  scrollEnabled: true,
  backgroundColor: 'black',
  prevRouteEntry: null,
  language: '',
})
