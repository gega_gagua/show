import { screens, colors } from '@utils/tailwind'
import {
  WHITE,
  BLACK,
  PUTTY,
  TRANSPARENT,
  TRANSPARENT_REVERSE,
} from '@c/Block/BlockMenuBar'
import {
  WHITE as CURSOR_WHITE,
  BLACK as CURSOR_BLACK,
} from '@c/Core/CoreCursorSignpost'

export const MUTATIONS = {
  SET_PREV_ROUTE_ENTRY: 'SET_PREV_ROUTE_ENTRY',
  SET_IS_MOBILE: 'SET_IS_MOBILE',
  SET_DEVICE_WIDTH: 'SET_DEVICE_WIDTH',
  SET_DEVICE_HEIGHT: 'SET_DEVICE_HEIGHT',
  SET_DPR: 'SET_DPR',
  SET_MENU_OPEN: 'SET_MENU_OPEN',
  SET_CURSOR_SIGNPOST: 'SET_CURSOR_SIGNPOST',
  UNSET_CURSOR_SIGNPOST: 'UNSET_CURSOR_SIGNPOST',
  SET_BREADCRUMB: 'SET_BREADCRUMB',
  UNSET_BREADCRUMB: 'UNSET_BREADCRUMB',
  SET_SCROLL_DATA: 'SET_SCROLL_DATA',
  SET_SCROLL_ENABLED: 'SET_SCROLL_ENABLED',
  UNSET_SCROLL_DATA: 'UNSET_SCROLL_DATA',
  SET_MENU_WHITE: 'SET_MENU_WHITE',
  SET_MENU_BLACK: 'SET_MENU_BLACK',
  SET_MENU_PUTTY: 'SET_MENU_PUTTY',
  SET_MENU_TRANSPARENT: 'SET_MENU_TRANSPARENT',
  SET_MENU_TRANSPARENT_REVERSE: 'SET_MENU_TRANSPARENT_REVERSE',
  SET_CURSOR_SIGNPOST_WHITE: 'SET_CURSOR_SIGNPOST_WHITE',
  SET_CURSOR_SIGNPOST_BLACK: 'SET_CURSOR_SIGNPOST_BLACK',
  SET_CURSOR_INTERACTING: 'SET_CURSOR_INTERACTING',
  SET_WHEEL_ENABLED: 'SET_WHEEL_ENABLED',
  SET_TOUCH_ENABLED: 'SET_TOUCH_ENABLED',
  SET_BACKGROUND_COLOR: 'SET_BACKGROUND_COLOR',
  SET_BACKGROUND_COLOR_ONLY: 'SET_BACKGROUND_COLOR_ONLY',
  SET_LANGUAGE: 'SET_LANGUAGE',
}

export default {
  /**
   * prevRouteEntry
   * @param {*} state
   */
  [MUTATIONS.SET_PREV_ROUTE_ENTRY](state, payload) {
    state.prevRouteEntry = payload
  },

  /**
   * isMobile
   * @param {*} state
   */
  [MUTATIONS.SET_IS_MOBILE](state) {
    state.isMobile =
      window.innerWidth <
      parseInt(screens().find((breakpoint) => breakpoint.handle === 'lg').width)
  },

  /**
   * Set device inner width
   * @param {*} state
   */
  [MUTATIONS.SET_DEVICE_WIDTH](state) {
    state.deviceWidth = window.innerWidth
  },

  /**
   * Set device inner height
   * @param {*} state
   */
  [MUTATIONS.SET_DEVICE_HEIGHT](state) {
    const h = window.innerHeight

    state.deviceHeight = h || document.documentElement.clientHeight
  },

  /**
   * Set device DPR
   * @param {*} state
   */
  [MUTATIONS.SET_DPR](state) {
    state.dpr = window.devicePixelRatio
  },

  /**
   * Management of mobile menu openness amd side effects
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.SET_MENU_OPEN](state, payload) {
    state.menuOpen = payload
    document.body.classList[payload ? 'add' : 'remove']('overflow-hidden')
  },

  /**
   * Set symbol for cursor signpost
   */
  [MUTATIONS.SET_CURSOR_SIGNPOST](state, payload) {
    state.cursorSignpost = payload
  },

  /**
   * Reset cursor signpost
   */
  [MUTATIONS.UNSET_CURSOR_SIGNPOST](state) {
    state.cursorSignpost = null
  },

  /**
   * Set boolean of cursor interacting with elements
   */
  [MUTATIONS.SET_CURSOR_INTERACTING](state, payload) {
    state.cursorInteracting = payload
  },

  /**
   * Set symbol for section breadcrumb
   */
  [MUTATIONS.SET_BREADCRUMB](state, payload) {
    state.breadcrumb = payload
  },

  /**
   * Reset symbol for section breadcrumb
   */
  [MUTATIONS.UNSET_BREADCRUMB](state) {
    state.breadcrumb = null
  },

  /**
   * Set scroll data for home scroll
   */
  [MUTATIONS.SET_SCROLL_DATA](state, payload) {
    state.scrollData = payload
  },

  /**
   * Reset scroll data for home scroll
   */
  [MUTATIONS.UNSET_SCROLL_DATA](state) {
    state.scrollData = null
  },

  /**
   * Set color for menu bar
   */
  [MUTATIONS.SET_MENU_WHITE](state) {
    state.menuBgColor = WHITE
  },

  /**
   * Set color for menu bar
   */
  [MUTATIONS.SET_MENU_BLACK](state) {
    state.menuBgColor = BLACK
  },

  /**
   * Set color for menu bar
   */
  [MUTATIONS.SET_MENU_PUTTY](state) {
    state.menuBgColor = PUTTY
  },

  /**
   * Set color for menu bar
   */
  [MUTATIONS.SET_MENU_TRANSPARENT](state) {
    state.menuBgColor = TRANSPARENT
  },

  /**
   * Set color for menu bar
   */
  [MUTATIONS.SET_MENU_TRANSPARENT_REVERSE](state) {
    state.menuBgColor = TRANSPARENT_REVERSE
  },

  /**
   * Set color for cursor signpost
   */
  [MUTATIONS.SET_CURSOR_SIGNPOST_WHITE](state) {
    state.cursorSignpostColor = CURSOR_WHITE
  },

  /**
   * Set color for cursor signpost
   */
  [MUTATIONS.SET_CURSOR_SIGNPOST_BLACK](state) {
    state.cursorSignpostColor = CURSOR_BLACK
  },

  /**
   * Set wheel enabled
   */
  [MUTATIONS.SET_WHEEL_ENABLED](state) {
    state.wheelEnabled = true
  },

  /**
   * Set wheel enabled
   */
  [MUTATIONS.SET_TOUCH_ENABLED](state) {
    state.touchEnabled = true
  },

  /**
   * Set scroll enabled
   * @param {*} state
   * @param {*} payload
   */
  [MUTATIONS.SET_SCROLL_ENABLED](state, payload) {
    state.scrollEnabled = payload
  },
  /**
   * * Set background color
   */
  [MUTATIONS.SET_BACKGROUND_COLOR](state, payload) {
    state.backgroundColor = payload
    document.body.style.backgroundColor = colors[payload]
  },
  /**
   * * Set background color
   */
  [MUTATIONS.SET_BACKGROUND_COLOR_ONLY](state, payload) {
    state.backgroundColor = payload
  },
  /**
   * Set language
   */
  [MUTATIONS.SET_LANGUAGE](state, payload) {
    state.language = payload
  },
  /**
   * reSet language
   */
  [MUTATIONS.RESET_LANGUAGE](state) {
    state.language = 'en'
  },
}
