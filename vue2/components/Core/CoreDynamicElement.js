export const CoreDynamicElementLite = {
  functional: true,

  props: {
    type: {
      default: 'div',
    },
  },

  render(createElement, { props, data, slots }) {
    return createElement(
      props.type,
      {
        class: {
          ...(data.staticClass && {
            [data.staticClass]: true,
          }),
        },
        attrs: {
          ...data.attrs,
        },
      },
      slots.default
    )
  },
}

export default {
  props: {
    type: {
      default: 'div',
    },
  },

  render(createElement) {
    return createElement(this.type, this.$slots.default)
  },
}
