import { mapState } from 'vuex'
import gsap from 'gsap'
import { isUndefined } from '@utils/types'

// To Do: add reverse gear

export default {
  data: () => ({
    repeats: 1,
    // time it takes to move 1 one screen length
    speed: 120,
    tl: null,
    // is the mouse hovering the core marquee element
    hover: false,
  }),

  computed: {
    ...mapState({
      deviceWidth: (state) => state.ui.deviceWidth,
    }),
  },

  watch: {
    deviceWidth() {
      this.setRepeats()
    },
    // watch whether user is hovering over the marquee and change the timeline animation (gsap)
    hover(status) {
      // status
      //   ? gsap.to(this.tl, { timeScale: 0, duration: 3 })
      //   : gsap.to(this.tl, { timeScale: 1, duration: 1 })
      status ? this.tl.pause() : this.tl.resume()
    },
  },

  mounted() {
    this.setRepeats()
    this.interactionEvents()

    this.observer = new MutationObserver(this.setRepeats)

    // Setup the observer
    this.observer.observe(this.$refs.parent, {
      attributes: false,
      childList: true,
      characterData: true,
      subtree: true,
    })
  },

  beforeDestroy() {
    this.observer.disconnect()
  },

  methods: {
    setRepeats() {
      const screenMultiplications = 25

      const elmWidth = this.$refs.parent.firstElementChild.firstElementChild.getBoundingClientRect()
        .width

      const repeats = Math.ceil(
        (this.deviceWidth / elmWidth) * screenMultiplications
      )

      // Cancel last tween, if it exists
      if (!isUndefined(this.tween)) {
        gsap.killTweensOf(this.$refs.child)
      }

      // work out how much you need to multiply it by to get 24 * screen width
      const animationRepeats =
        (repeats / screenMultiplications) * (screenMultiplications - 1)

      this.tl = gsap.timeline()

      this.tl.fromTo(
        this.$refs.child,
        { x: 0 },
        {
          x: -animationRepeats * elmWidth,
          duration: this.speed * animationRepeats,
          repeat: -1,
        }
      )

      if (this.repeats !== repeats && isFinite(repeats)) {
        this.repeats = repeats
      }
    },
    interactionEvents() {
      // Pause marquee on hover
      this.$refs.parent.addEventListener('mouseenter', () => {
        // console.log('mouseenter', this.$el)
        // gsap.to(this.tl, { timeScale: 0, duration: 3 })
        // this.tl.pause()
      })
      // Resumes marquee when not hovering
      this.$refs.parent.addEventListener('mouseleave', () => {
        // console.log('mouseleave', this.$el)
        // gsap.to(this.tl, { timeScale: 1, duration: 1 })
        // this.tl.play()
      })
    },
  },

  render(h) {
    return h(
      'div',
      {
        class: 'overflow-x-hidden overflow-y-hidden w-full',
        ref: 'parent',
        on: {
          mouseenter: () => {
            this.hover = true
          },
          mouseleave: () => {
            this.hover = false
          },
        },
      },
      [
        h(
          'div',
          {
            class: 'flex',
            style: {
              width: '3600vw',
            },
            key: this.repeats,
            ref: 'child',
          },
          new Array(this.repeats).fill(this.$slots.default)
        ),
      ]
    )
  },
}
