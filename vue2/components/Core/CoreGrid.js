import CoreDynamicElement from './CoreDynamicElement'

const lgColumns = {
  1: 'lg:grid-cols-1',
  2: 'lg:grid-cols-2',
  3: 'lg:grid-cols-3',
  4: 'lg:grid-cols-4',
  5: 'lg:grid-cols-5',
  6: 'lg:grid-cols-6',
  7: 'lg:grid-cols-7',
  8: 'lg:grid-cols-8',
  9: 'lg:grid-cols-9',
  10: 'lg:grid-cols-10',
  11: 'lg:grid-cols-11',
  12: 'lg:grid-cols-12',
  13: 'lg:grid-cols-13',
  14: 'lg:grid-cols-14',
  15: 'lg:grid-cols-15',
  16: 'lg:grid-cols-16',
  17: 'lg:grid-cols-17',
  18: 'lg:grid-cols-18',
}

export default {
  props: {
    lgColumns: {
      type: Number,
      default: 18,
    },
    lgOnly: {
      type: Boolean,
      default: false,
    },
    elmType: {
      type: String,
      default: 'div',
    },
  },

  render(createElement) {
    return createElement(
      CoreDynamicElement,
      {
        class: [
          'lg:gap-x-gutter-desk',
          lgColumns[this.lgColumns],
          this.lgOnly
            ? 'lg:grid lg:grid-cols-6 lg:gap-x-gutter-mob'
            : 'grid grid-cols-6 gap-x-gutter-mob',
        ],
        props: { type: this.elmType },
      },
      this.$slots.default
    )
  },
}
