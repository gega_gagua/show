// EXAMPLE USEAGE
// import { READ_MORE } from '@c/Core/CoreCursorSignpost'
// import { MUTATIONS } from '@store/ui'

// export default {
//   mounted() {
//     this.$store.commit(MUTATIONS.SET_CURSOR_SIGNPOST, READ_MORE)
//   },

//   beforeDestroy() {
//     this.$store.commit(MUTATIONS.UNSET_CURSOR_SIGNPOST)
//   },
// }

import { mapState } from 'vuex'
import { ACTIONS } from '@store/animation'
import { bind, unbind } from '@utils/eventListener'

export const READ_MORE = Symbol('cursor string')
export const VIEW_PROJECT = Symbol('cursor string')
export const ABOUT = Symbol('cursor string')

export const WHITE = Symbol('cursor colour')
export const BLACK = Symbol('cursor colour')

const STRING_LITERALS = {
  [READ_MORE]: 'READ MORE',
  [VIEW_PROJECT]: 'VIEW PROJECT',
  [ABOUT]: 'ABOUT',
}

export default {
  data: () => ({
    animationUuid: null,
    clientX: 0,
    clientY: 0,
  }),

  computed: {
    ...mapState({
      cursorSignpost: (state) => state.ui.cursorSignpost,
      cursorSignpostColor: (state) => state.ui.cursorSignpostColor,
    }),
    text() {
      return STRING_LITERALS?.[this.cursorSignpost]
        ? STRING_LITERALS[this.cursorSignpost]
        : ''
    },
    cursorColor() {
      return this.cursorSignpostColor === WHITE ? 'text-white' : 'text-black'
    },
  },

  watch: {
    text: {
      immediate: true,
      async handler(text) {
        if (text !== '') {
          // Dispatch animation frame
          this.animationUuid = await this.$store.dispatch(
            ACTIONS.SUBSCRIBE,
            this.animate
          )

          // Bind mousemove event
          bind(window, 'mousemove', this.handleMousemove)

          return
        }

        if (text === '' && this.animationUuid !== null) {
          // Kill animation frame
          this.$store.dispatch(ACTIONS.UNSUBSCRIBE, this.animationUuid)
          this.animationUuid = null

          // Unbind mousemove
          unbind(window, 'mousemove', this.handleMousemove)
        }
      },
    },
  },

  methods: {
    animate() {
      // Peform this update in an RAF rather than computed
      this.$refs.signpost.style.transform = `translate(${this.clientX}px, ${this.clientY}px) translateY(-50%) translateX(13px)`
    },
    handleMousemove(e) {
      this.clientX = e.clientX
      this.clientY = e.clientY
    },
  },

  render(createElement) {
    return createElement(
      'span',
      {
        class: [
          'hidden lg:block pointer-events-none select-none fixed top-0 left-0 font-euclid-regular text-size-1',
          this.cursorColor,
        ],
        ref: 'signpost',
      },
      this.text
    )
  },
}
