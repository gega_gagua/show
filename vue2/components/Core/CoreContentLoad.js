import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { CustomEase } from 'gsap/CustomEase'

gsap.registerPlugin(CustomEase)
gsap.registerPlugin(ScrollTrigger)

CustomEase.create('content-load', '0, 0, 0.2, 1')

export default {
  props: {
    delay: {
      type: Number,
      default: 0,
    },
    enabled: {
      type: Boolean,
      default: true,
    },
  },

  data: () => ({
    observer: null,
  }),

  mounted() {
    if (!this.enabled) return

    this.observer = new IntersectionObserver(this.handleIntersection, {})

    this.observer.observe(this.$el)

    gsap.set(this.$el, {
      autoAlpha: 0,
      y: 100,
    })

    // const tl = gsap.timeline({
    //   scrollTrigger: {
    //     trigger: this.$el,
    //     once: false,
    //     toggleActions: 'play reverse play reverse',
    //   },
    // })

    // tl.from(this.$el, {
    //   autoAlpha: 0,
    //   y: 100,
    //   duration: 1,
    //   ease: 'content-load',
    //   delay: 0.1 * this.delay,
    // })
  },

  beforeDestroy() {
    // gsap.killTweensOf(this.$el)
    if (!this.observer) return

    this.observer.disconnect()
  },

  methods: {
    handleIntersection(entries) {
      const notBelowScreen =
        entries[0].boundingClientRect.y <= window.innerHeight

      if (notBelowScreen) {
        gsap.to(this.$el, {
          autoAlpha: 1,
          y: 0,
          duration: 1,
          ease: 'content-load',
          delay: 0.1 * this.delay,
        })
      } else {
        // console.log('here')
        gsap.killTweensOf(this.$el)
        gsap.set(this.$el, {
          autoAlpha: 0,
          y: 100,
        })
      }
    },
  },

  render() {
    return this.$slots.default
  },
}
