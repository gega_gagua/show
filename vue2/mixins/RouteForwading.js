export default {
  created() {
    const {
      params: { routeForwading, routeForwadingSlug },
      name,
    } = this.$route

    if (routeForwading) {
      // 1000 is --hover-long timing
      setTimeout(
        () =>
          this.$router.push({
            name: `${name}-slug`,
            params: { slug: routeForwadingSlug },
          }),
        1000
      )
    }
  },
}
