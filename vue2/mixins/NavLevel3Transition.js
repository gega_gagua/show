export default {
  transition(to, from) {
    if (!from) return
    if (from.path !== '/' && to.path !== '/') {
      return {
        name: 'route-transition-fade',
        mode: '',
      }
    }
  },
}
