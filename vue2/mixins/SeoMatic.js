// Expects the following structure from SEOMatic
// seomatic(uri: "/", asArray: true) {
//   metaTitleContainer
//   metaTagContainer
//   metaLinkContainer
//   metaScriptContainer
//   metaJsonLdContainer
// }

export default {
  data: () => ({
    seoTitle: null,
    seoMeta: null,
    seoScript: null,
    seoLink: null,
  }),

  methods: {
    generateTags(data, titleProp) {
      const {
        metaTitleContainer: {
          title: { title },
        },
        metaTagContainer,
        // metaLinkContainer,
        // metaJsonLdContainer,
      } = Object.entries(data).reduce((acc, [key, value]) => {
        if (key !== '__typename') {
          acc[key] = JSON.parse(value)
          return acc
        }
        return acc
      }, {})

      if (titleProp) {
        this.seoTitle = titleProp
      } else {
        this.seoTitle = title
      }

      // Flatten metaTagContainer values into string
      this.seoMeta = metaTagContainer
        ? Object.values(metaTagContainer).reduce(
            (flat, next) => flat.concat(next),
            []
          )
        : null

      // Flatten metaLinkContainer values into string
      // this.seoLink = metaLinkContainer
      //   ? Object.values(metaLinkContainer).reduce(
      //       (flat, next) => flat.concat(next),
      //       []
      //     )
      //   : null

      // Convert JsonLd to <script type="application/ld+json">...</script>
      // this.seoScript = metaJsonLdContainer
      //   ? Object.entries(metaJsonLdContainer).map((value) => ({
      //       type: 'application/ld+json',
      //       innerHTML: JSON.stringify(value),
      //     }))
      //   : []
    },
  },

  head() {
    return {
      meta: this.seoMeta,
      title: this.seoTitle,
      link: [
        {
          rel: 'canonical',
          href: `https://mawd.co${this.$route.path}`,
        },
      ],
      // link: this.seoLink,
      // script: this.seoScript,
    }
  },
}
