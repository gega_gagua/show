import { MUTATIONS } from '@store/ui'
import { mapState } from 'vuex'
import {
  HOME,
  OVERVIEW,
  OUR_PROCESS,
  OUR_WORK,
  CLIENTS,
  TESTIMONIALS,
  CONTACT,
} from '@c/Core/CoreChineseBreadcrumb'

export default {
  data: () => ({
    breadcrumbs: [
      HOME,
      OVERVIEW,
      OUR_PROCESS,
      OUR_WORK,
      CLIENTS,
      TESTIMONIALS,
      CONTACT,
    ],
  }),

  computed: {
    ...mapState({
      deviceHeight: (state) => state.ui.deviceHeight,
      isMobile: (state) => state.ui.isMobile,
    }),
  },

  methods: {
    scrollSpine(params) {
      const amt = -params.step * this.deviceHeight
      this.$store.commit(MUTATIONS.SET_SCROLL_DATA, {
        anchor: params.anchor,
        step: params.step,
        amt,
        subScrollStep: 0,
        reversed: false,
      })
      this.$router.push({ name: 'index', hash: params.anchor })
      this.$store.commit(
        MUTATIONS.SET_BREADCRUMB,
        this.breadcrumbs[params.step]
      )
      // console.log('breadcrumb = ', this.breadcrumbs[params.step])

      if (this.isMobile) {
        params.color.mobile === 'white'
          ? this.$store.commit(MUTATIONS.SET_MENU_TRANSPARENT_REVERSE)
          : this.$store.commit(MUTATIONS.SET_MENU_TRANSPARENT)
        this.$store.commit(
          MUTATIONS.SET_BACKGROUND_COLOR_ONLY,
          params.background.mobile
        )
      } else {
        params.color.desktop === 'white'
          ? this.$store.commit(MUTATIONS.SET_MENU_TRANSPARENT_REVERSE)
          : this.$store.commit(MUTATIONS.SET_MENU_TRANSPARENT)
        this.$store.commit(
          MUTATIONS.SET_BACKGROUND_COLOR_ONLY,
          params.background.desktop
        )
      }
    },
  },
}
